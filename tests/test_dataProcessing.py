import pytest
# from context import dataProcessing
from dataProcessing import DataProcessing

class TestGetAPIValidity:
    """
    AIM: To test the method DataProcessing.get_api_validity()
    EXPECTED RESULTS: True if API key is valid. 
    COMMAND TO RUN TEST: pytest test_dataProcessing.py
    OBTAINED RESULT: Passing
    """

    def test__get_api_validity__return_True(self):
        dat_obj = DataProcessing()
        resp = dat_obj.get_api_validity()
        assert resp is True
    def test__get_api_validity__return_False(self):
        dat_obj = DataProcessing()
        dat_obj.MAPI_KEY="x"
        assert dat_obj.get_api_validity() is False


class TestGetElementAIABEnergy:
    """
    AIM: To test the method DataProcessing.get_element_aiab_energy()
    EXPECTED RESULTS: Atom-in-a-box energies of specified elements.
    COMMAND TO RUN TEST: pytest test_dataProcessing.py
    OBTAINED RESULT: Passing
    """
    def test__get_element_aiab_energy__error(self):
        dat_obj = DataProcessing(
            check_api=False
        )
        assert (dat_obj.get_element_aiab_energy('Pr')==-0.55838298) is True
    def test__get_element_aiab_energy__wrong_entity(self):
        dat_obj = DataProcessing(
            check_api=False
        )
        assert (dat_obj.get_element_aiab_energy('Yb')==-0.55838298) is False



