import pytest
from localRegressionSimple import LocalRegressionSimple
import numpy as np 
lr_obj = LocalRegressionSimple(
    frac = 0.3
)
class TestEvalDist:
    """
        AIM: To test the method LocalRegressionSimple.eval_weights()
        EXPECTED RESULTS: Evaluate distances from a given point in a given window.
        COMMAND TO RUN TEST: pytest test_localRegressionSimple.py
        OBTAINED RESULT: Passing
    """
    def test__given_values__return_dist(self):
        lr_obj = LocalRegressionSimple(
            frac = 0.3
        )
        assert (np.array_equal(
            np.array([2,1,0,1,2],dtype=float),
            lr_obj.eval_dist([-2,-1,0,1,2],0)
        )) is True
    def test__given_one_value__return_zero(self):
        lr_obj = LocalRegressionSimple(
            frac = 0.3
        )
        assert (np.array_equal(
            np.array([0],dtype=float),
            lr_obj.eval_dist([1],1)
        )) is True
    def test__given_all_same__return_zero_array(self):
        lr_obj = LocalRegressionSimple(
            frac = 0.3
        )
        assert (np.array_equal(
            np.array(5*[0],dtype=float),
            lr_obj.eval_dist([2,2,2,2,2],2)
        )) is True

class TestEvalWeights:
    """
    AIM: To test the method LocalRegressionSimple.eval_weights()
    EXPECTED RESULTS: Evaluate weights from normalized distances according to 
        tricube weight function in a given window.
    COMMAND TO RUN TEST: pytest test_localRegressionSimple.py
    OBTAINED RESULT: Passing
    """
    def test__given_distance_1__return_0(self):
        lr_obj = LocalRegressionSimple(
            frac = 0.3
        )
        assert (np.array_equal(
            np.array([0],dtype=float),
            lr_obj.eval_weights([1])
        )) is True
    def test__given_distance_0__return_1(self):
        lr_obj = LocalRegressionSimple(
            frac = 0.3
        )
        assert (np.array_equal(
            np.array([1],dtype=float),
            lr_obj.eval_weights([0])
        )) is True
    def test__given_distance_more_than_1__return_values(self):
        lr_obj = LocalRegressionSimple(
            frac = 0.3
        )
        assert (np.array_equal(
            np.array([-343],dtype=float),
            lr_obj.eval_weights([2])
        )) is True
    