import pytest
import numpy as np
from gradientMachine import GradientBoostingMachine

class TestSquaredError:
    """
    AIM: To test the method GradientBoostingMachine.squared_error()
    EXPECTED RESULTS: Return evaluated squared error between two arrays.
    COMMAND TO RUN TEST: pytest test_gradientMachine.py
    OBTAINED RESULT: Passing
    """
    def test__given_same__return_zero(self):
        gbm = GradientBoostingMachine(verbose=False,max_comps=1,init_iterations=1)
        assert (gbm.squared_error(1,1)==0.0) 
    
    def test__given_values__return_answer_true(self):
        gbm = GradientBoostingMachine(verbose=False,max_comps=1,init_iterations=1)
        assert (gbm.squared_error(1,0)==0.5) 
class TestRMSE:
    """
    AIM: To test the method GradientBoostingMachine.rmse()
    EXPECTED RESULTS: Return evaluated root mean squared error between two arrays.
    COMMAND TO RUN TEST: pytest test_gradientMachine.py
    OBTAINED RESULT: Passing
    """
    def test__given_same__return_zero(self):
        gbm = GradientBoostingMachine(verbose=False,max_comps=1,init_iterations=1)
        assert (gbm.rmse(np.array([1,1]),np.array([1,1]))==0.0) 
    def test__given_one__raise_AttributeError(self):
        gbm = GradientBoostingMachine(verbose=False,max_comps=1,init_iterations=1)
        with pytest.raises(AttributeError):
            assert (gbm.rmse(1,1)==0.0)