import pytest
from json import load
from hoelderMeans import HoelderMeans

class TestEvalMeans:
    """
    AIM: To test the method HoelderMeans.eval_means()
    EXPECTED RESULTS: Evaluate hoelder means if files are not found on disk.
    COMMAND TO RUN TEST: pytest test_hoelderMeans.py
    OBTAINED RESULT: Passing
    """
    def test__eval_means__return_0(self):
        hm_obj = HoelderMeans(
            comp_descriptors = [],
            verbose = False,
            source_file = "data_ncomp",
            file = "test_indep_data_ncomp",
            max_components = 1
        )
        resp = hm_obj.eval_means(
                source="data_ncomp_1.json",
                file_name="test_indep_data_ncomp_1.json"
        )
        assert resp is 0
    def test__eval_means_no_exponents__raises_TypeError(self):
        hm_obj = HoelderMeans(
            comp_descriptors = [],
            verbose = False,
            source_file = "data_ncomp",
            file = "test_indep_data_ncomp",
            max_components = 1
        )
        with pytest.raises(TypeError):
            resp = hm_obj.eval_means(
                source="data_ncomp_1.json",
                file_name="test_indep_data_ncomp_1.json",
                exponents=None
            )
    def test__eval_means_given_wrong_source__raises_AttributeError(self):
        hm_obj = HoelderMeans(
            comp_descriptors = [],
            verbose = False,
            source_file = "data_ncomp",
            file = "test_indep_data_ncomp",
            max_components = 1
        )
        with pytest.raises(AttributeError):
            resp = hm_obj.eval_means(
                source="data_ncomp_.json",
                file_name="test_indep_data_ncomp_1.json"
            )
class TestGetAlpha:
    """
    AIM: To test the method HoelderMeans.get_alpha()
    EXPECTED RESULTS: Evaluate the multiplier used to compute hoelder means data. 
    COMMAND TO RUN TEST: pytest test_hoelderMeans.py
    OBTAINED RESULT: Passing
    """
    def test__given_one_value_no_weights__return_value(self):
        hm_obj = HoelderMeans(
            comp_descriptors = [],
            verbose = False,
            source_file = "data_ncomp",
            file = "test_indep_data_ncomp",
            max_components = 1
        )
        assert (hm_obj.get_alpha(weights=None,norm_method='max',values=[1])==1.0) is True
    
    def test__given_two_values_and_no_weights__return_average(self):
        hm_obj = HoelderMeans(
            comp_descriptors = [],
            verbose = False,
            source_file = "data_ncomp",
            file = "test_indep_data_ncomp",
            max_components = 1
        )
        assert (hm_obj.get_alpha(weights=None,norm_method='max',values=[1,2])==0.5) is True
        
    def test__given_one_value_with_two_weights__return_zero(self):
        hm_obj = HoelderMeans(
            comp_descriptors = [],
            verbose = False,
            source_file = "data_ncomp",
            file = "test_indep_data_ncomp",
            max_components = 1
        )
        assert (hm_obj.get_alpha(weights=[1,2],norm_method='max',values=[1])==0.0) is True
        
    def test__given_unknown_normalization_method__return_0(self):
        hm_obj = HoelderMeans(
            comp_descriptors = [],
            verbose = False,
            source_file = "data_ncomp",
            file = "test_indep_data_ncomp",
            max_components = 1
        )
        assert (hm_obj.get_alpha(weights=[1,2],norm_method='inv',values=[1,2])==0.0) is True
        
    def test__given_no_values__raise_TypeError(self):
        hm_obj = HoelderMeans(
            comp_descriptors = [],
            verbose = False,
            source_file = "data_ncomp",
            file = "test_indep_data_ncomp",
            max_components = 1
        )
        with pytest.raises(TypeError):
            assert (hm_obj.get_alpha(weights=[1,2],norm_method='max')==0.0) is True
        
class TestComputeMean:
    """
    AIM: To test the method HoelderMeans.compute_mean()
    EXPECTED RESULTS: Compute hoelder means if not found on disk. 
    COMMAND TO RUN TEST: pytest test_hoelderMeans.py
    OBTAINED RESULT: Passing
    """
    def test__given_no_exponent_raise_ValueError(self):
        hm_obj = HoelderMeans(
            comp_descriptors = [],
            verbose = False,
            source_file = "data_ncomp",
            file = "test_indep_data_ncomp",
            max_components = 1
        )
        with pytest.raises(ValueError):
            hm_obj.compute_mean(values=[1,2,3],weights=[0.5,0.5,0.5])
    def test__given_exponent_1__return_arithmetic_mean(self):
        hm_obj = HoelderMeans(
            comp_descriptors = [],
            verbose = False,
            source_file = "data_ncomp",
            file = "test_indep_data_ncomp",
            max_components = 1
        )
        assert (int(hm_obj.compute_mean(values=[1,2,3],weights=[1,1,1],exponent=1))==2) is True
    def test__given_exponent_0__return_geometric_mean(self):
        hm_obj = HoelderMeans(
            comp_descriptors = [],
            verbose = False,
            source_file = "data_ncomp",
            file = "test_indep_data_ncomp",
            max_components = 1
        )
        assert (int(hm_obj.compute_mean(values=[1,2,4],exponent=0))==1) is True

class TestCombineDFTData:
    """
    AIM: To test the method HoelderMeans.combine_dft_data()
    EXPECTED RESULTS: Combine evaluated hoelder means data and data obtained from DFT for 
        specific descriptors. 
    COMMAND TO RUN TEST: pytest test_hoelderMeans.py
    OBTAINED RESULT: Passing
    """
    def test__given_key_return_value__assert_true(self):
        with open("../data/indep_ncomp_2.json",'r') as inp:
            data = load(inp)
        assert (data['mp-866199']['e_above_hull']==0.0) is True
    def test__given_key_return_value__assert_false(self):
        with open("../data/indep_ncomp_2.json",'r') as inp:
            data = load(inp)
        assert (data['mp-866199']['e_above_hull']==1.0) is False
    def test__given_invalid_key__raise_KeyError(self):
        with open("../data/indep_ncomp_2.json",'r') as inp:
            data = load(inp)
        with pytest.raises(KeyError):
            assert (data['mp-866f9']['e_above_hull']==0.0) is True
        
class TestWriteToFile:
    """
    AIM: To test the method HoelderMeans.write_to_file()
    EXPECTED RESULTS: Write the combined hoelder means data as independent 
        variables data to disk. 
    COMMAND TO RUN TEST: pytest test_hoelderMeans.py
    OBTAINED RESULT: Passing
    """
    def test__given_no_file_name__raise_TypeError(self):
        with pytest.raises(TypeError):
            hm_obj = HoelderMeans(
                comp_descriptors = [],
                verbose = False,
                source_file = "data_ncomp",
                file = None,
                max_components = 1
            )

    def test__loading_values_post_write__True_case(self):
        with open("../data/indep_ncomp_1.json",'r') as inp:
            data = load(inp)
        assert (data['mp-862690']['atomic_number_neg_4']==89.0) is True

    def test__loading_values_post_write__False_case(self):
        with open("../data/indep_ncomp_1.json",'r') as inp:
            data = load(inp)
        assert (data['mp-10018']['group_neg_3']==89.0) is False

    def test__loading_values_post_write__raise_KeyError(self):
        with open("../data/indep_ncomp_1.json",'r') as inp:
            data = load(inp)
        with pytest.raises(KeyError):
            assert (data['mp-866199']['atomic_number_neg_4']==89.0) is True