Source
======

.. toctree::
   :maxdepth: 4

   dataProcessing
   hoelderMeans
   localRegression
   localRegressionSimple
   localRegressionCython
   gradientMachine
   plot
   profiler
   train