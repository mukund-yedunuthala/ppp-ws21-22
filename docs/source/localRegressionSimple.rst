Local Regression Simplified
===========================
.. automodule:: localRegressionSimple
   :members:
   :undoc-members:
   :show-inheritance: