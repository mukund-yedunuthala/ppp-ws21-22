.. Gradient Boosting Machine with Local Regression weak learner documentation master file, created by
   sphinx-quickstart on Tue Jun 28 15:34:51 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Gradient Boosting Machine with Local Regression weak learner
============================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
