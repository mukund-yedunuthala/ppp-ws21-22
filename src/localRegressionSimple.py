"""**Implementation of Multi-variate local polynomial regression.**

- This file is a part of *Personal Programming Project (PPP)* coursework in \
Computational Materials Science (CMS) M.Sc. course in *Technische Universität \
Bergakademie Freiberg.*
- This file is a part of the project titled **Application of statistical learning \
to predict material properties.**
- For a given number of data points, the algorithm fits a polynomial of given \
degree to the data points in a specified neighbourhood. 
- Given response variables and predictor variables, this can be used to estimate \
a regression function. 
- At fitting points the underlying function is assumed to be smooth and k-times \
differentiable to fit a polynomial of degree k.

References
----------
    Cleveland, W.S. (1979) "Robust Locally Weighted Regression
    and Smoothing Scatterplots". Journal of the American Statistical
    Association 74 (368): 829-836.

"""

import numpy as np

__author__ = "Venkata Mukund Kashyap Yedunuthala"


class LocalRegressionSimple:
    """Multi-variate local polynomial regression.

    Methods
    -------

    :meth:`eval_dist` : Method that computes the distances of a given point
        and its counterparts in a specified window.

    :meth:`eval_weights` : Method that computes the weights from the
        normalized distances within the window. This method specifically
        returns values of tricube weighting function evaluation.

    :meth:`fit` : Method that provides the initial wrapper for the methods
        mentioned above.

    :meth:`predict` : Method that utilizes the learned data matrix to
        evaluate the predictor variables.

    Attributes
    ----------
    frac : float
        Fraction of data to be used while estimating polynomial.

    """

    def __init__(self, frac=None):
        """To instantiate an object of class LocalRegressionSimple.

        Attributes
        ----------
        frac : float
            Fraction of data to be used to determine the size of the
            window. Defaults to 1/3.

        """
        if frac is None:
            self.frac = 1 / 3
        else:
            self.frac = frac

    def eval_dist(self, window, x):
        """Method to evaluate distances between a point x and other points
        in a specified window.

        Parameters
        ----------
        window : array
            Array of points from which distance is to be determined.
        x : array
            Point from which distances are to be determined.

        Returns
        -------
        dist : array
            Array with distances in the window.
        """
        dist = []
        for point in window:
            dist.append(np.linalg.norm(point - x))
        return np.array(dist, dtype=float)

    def eval_weights(self, norm_dist):
        """Method to evaluate weights using tricube weight function
        given normalized distances.

        Parameters
        ----------
        norm_dist : array
            Array consisting of normalized distances.

        Returns
        -------
        weights : array
            Array consisting of determined weights.
        """
        weights = np.zeros_like(norm_dist)
        for i in range(weights.shape[0]):
            weights[i] = (1 - np.abs(norm_dist[i]) ** 3) ** 3
        return weights

    def fit(self, x, y):
        """Method that evaluates the fit using local polynomial regression.

        Parameters
        ----------
        x : array
            Independent variables.
        y : array
            Dependent variables.

        Returns
        -------
        d_mat : array
            Corresponding data matrix obtained after regressing given values.
        """

        if len(x.shape) == 1:
            A = np.vstack([x, np.ones(x.shape[0])]).T

        else:
            A = []
            for i, xi in enumerate(x):
                A.append(xi.tolist() + [1])
            A = np.array(A)

        bandwidth = int(np.ceil(self.frac * x.shape[0]))
        weights = np.zeros(x.shape[0])
        d_mat = []
        for i, xi in enumerate(x):
            A_copy = A.copy()
            w_copy = weights.copy()
            dist = self.eval_dist(x, xi)
            req_ind = np.isin(dist, dist[np.argsort(dist)][:bandwidth])
            dis_window = dist[req_ind]
            norm_dist = dis_window / dis_window.max()
            w_copy[req_ind] = self.eval_weights(norm_dist)
            # W = np.diag(w_copy)
            A_copy = A_copy * np.sqrt(w_copy)[:, np.newaxis]
            y_copy = y * np.sqrt(w_copy)
            betas = np.linalg.lstsq(A_copy, y_copy, rcond=None)[0]
            d_mat.append(betas)
        self.d_mat = np.array(d_mat)
        return d_mat

    def predict(self, x, d_mat=None):
        """Method that utilizes given data matrix  and independent variables
        to evaluate the predictor variables.

        Parameters
        ----------
        x : array
            Values of independent variables at which predictions are made.

        d_mat : array
            Corresponding data matrix, defaults to the data matrix obtained
            after fitting the data. Running :meth:`fit()` before this is
            absolutely essential.

        Returns
        -------
        y_pred : array
            Array consisting of predicted dependent variables.
        """
        if d_mat is None:
            d_mat = self.d_mat
        if len(x.shape) == 1:
            x = x[:, np.newaxis]
        n_params = x.shape[1]
        y_pred = np.zeros(x.shape[0])
        for i in range(y_pred.shape[0]):
            y_pred[i] = x[i].dot(d_mat[i, :n_params]) + d_mat[i, n_params]
        return y_pred
