"""**Import and parse data from the Materials Project database.**

- Queries REST API and parses JSON objects obtained. 
- This file is a part of *Personal Programming Project (PPP)* coursework in \
Computational Materials Science (CMS) M.Sc. course in *Technische Universität \
Bergakademie Freiberg.*
- This file is a part of the project titled **Application of statistical learning \
to predict material properties.**
"""

from collections import defaultdict
from json import dump, dumps, load
from os import environ
from pathlib import Path

from numpy import average, errstate, isnan, log10
from pymatgen.core.composition import Composition
from pymatgen.core.periodic_table import Element
from requests import get, post

__author__ = "Venkata Mukund Kashyap Yedunuthala"


class DataProcessing:
    """To query and handle data obtained from the Materials Project
    database.

    Methods
    -------
        :meth:`get_api_validity` : To check the validity of API key provided.
        :meth:`read_element_aiab_energy` : Method to parse the atom-in-a-box energies
            for different elements provided in the database.
        :meth:`get_element_aiab_energy` : Method that uses the data read to act as a getter
            for atom-in-a-box energy of an element.
        :meth:`collect_data_from_source` : Method that acts as a wrapper for all the other
            methods present in this class. One specifies criteria and properties used to
            query the API and the data obtained is written to disk.
        :meth:`read_from_file` : Method that acts as a setter for DATA when the data is
            accessible on the disk, prevents unnecessary calls to API.
        :meth:`query_property` : Method that uses the properties and criteria specified
            to POST a query to the REST API of Materials Project.
        :meth:`get_pymatgen_data` : Method that uses specimen obtained after querying to
            add certain elemental properties that are easily accessible from pymatgen
            wrapper on a local level.
        :meth:`eval_per_atom_data` : Method that computes cohesive energy of an atom as
            well as their log volume per atom.
        :meth:`get_elasticity_data` : Method that specifically isolates Voigt-Reuss-Hill
            averages of bulk modulus (K) and shear modulus (G) from the data that is
            available on the disk.
        :meth:`write_query_to_file` : Helper method to parse the JSON object received from
            server to write to disk as JSON files.
    """

    def __init__(self, verbose=False, check_api=True) -> None:
        """Method to instantiate an object of DataProcessing class.

        Attributes
        ----------
        MAPI_KEY : str
            API key obtained from Materials Project, read from environment
            variable of the same title. Configuration file may be used to
            set the environment variable.
        MAPI_ENDPOINT : str
            URL of the Materials Project API, used in each query.
        MAPI_VERSION: str
            Version of the API to be used by this project. 'v2' recommended.
        DATA_PATH: Path
            Path of the data directory handled using PathLib module to support
            code being OS agnostic.
        OUTPUT_PATH: Path
            Path to directory that holds logs and other outputs.
        verbose: bool
            Indicator for verbosity in console and for generating output logs.
        """

        self.MAPI_KEY = environ.get("MAPI_KEY", " ").strip()
        self.MAPI_ENDPOINT = "https://www.materialsproject.org/rest/"
        self.MAPI_VERSION = "v2/"
        self.DATA_PATH = Path("../data/")
        self.OUTPUT_PATH = Path("../output/")
        self.verbose = verbose
        self.read_element_aiab_energy()
        self.DATA = None
        if check_api:
            if verbose:
                print("API Key validity: ", self.get_api_validity())
            if not self.get_api_validity():
                raise ValueError("API Key invalid")
        else:
            if verbose:
                print("API check disabled.")

    def get_api_validity(self, MAPI_REQUEST="api_check") -> bool:
        """Method that queries the Materials Project API to verify the validity of
        the API key configured in this project.

        Parameters
        ----------
        self : Object of DataProcessing
        MAPI_REQUEST: str
            The request being implemented in this method, obtained from Materials
            Project API documentation.
            Refer: https://docs.materialsproject.org/open-apis/the-materials-api/

        Returns
        -------
        : bool
            Indicates the validity of response.
        """

        resp = get(
            self.MAPI_ENDPOINT + "v1/" + MAPI_REQUEST, params={"API_KEY": self.MAPI_KEY}
        )

        return resp.json()["api_key_valid"]

    def read_element_aiab_energy(self) -> None:
        """Reads the atom-in-a-box energy and saves it to the dictionary specified
        from a pre-sourced file situated in data directory.
        Source: https://github.com/materialsproject/gbml

        Parameters
        ----------
        self: Object of Class DataProcessing

        Returns
        ------

        sets the energies attribute.
        """

        try:
            with open(self.DATA_PATH / "element_aiab_energy.json") as aiab_file:
                self.energies = load(aiab_file)
        except:
            self.energies = None

    def get_element_aiab_energy(self, entity):
        """Fetches atom-in-a-box energy for specified entity.

        Parameters
        ----------
        entity: str
            Formula or the id of the entity being queried.

        Returns
        -------
        energy: double
            Atom-in-a-box energy.
        """

        if self.energies is not None:
            return self.energies[entity][0]
        else:
            return None

    def collect_data_from_source(
        self, properties, criteria, file_name, max_components, log_file=None
    ) -> None:
        """Method that calls query and write methods with properties and criteria provided.

        Parameters
        ----------
        self : DataProcessing object
        properties: list
            List of properties being queried.
        criteria : dict
            Dictionary consisting of criteria with which Materials Project database is queried.
        file_name : str
            Prefix to file that stores data post response from query.
        max_components : int
            Maximum number of unique specimen in an item.
        log_file : str
            Name for the file which stores the logs of this method.

        Returns
        -------
         : None
        """
        for n in range(max_components):
            file = file_name
            criteria["nelements"] = n + 1
            properties = properties
            file = file + f"_{n+1}.json"
            code = self.query_property(
                properties=properties, criteria=criteria, file_name=file
            )
            if self.verbose and code == 0:
                status = f"Queried database for {properties} with number of components in specimen: {n+1}"
                print(status)
                # data_object.write_str_to_file(log_file,status)

            elif self.verbose and code == 2:
                status = f"Source data file already exists at {self.DATA_PATH/file}"
                print(status)
                self.read_from_file(file)
                # data_object.write_str_to_file(log_file,status)
        return code

    def read_from_file(self, file) -> int:
        """Reads data from provided file and sets the attribute.

        Parameters
        ----------
        file : str
            Name of the file.

        Returns
        -------
            : int
            A status indicator.
        """

        if Path.exists(self.DATA_PATH / file):
            if self.verbose:
                print(f"Data loaded from {self.DATA_PATH/file}")
            with open(self.DATA_PATH / file, "r") as ip:
                data = load(ip)
                self.DATA = data
            return 0
        else:
            if self.verbose:
                print(f"File not found at {self.DATA_PATH/file}")
            return -1

    def query_property(self, properties, criteria, file_name) -> int:
        """Method to query using Materials API's functionality for flexible
        queries.
        Refer https://docs.materialsproject.org/open-apis/the-materials-api/
        Calls :meth:`write_query_to_file()`.

        Parameters
        ----------
        properties : list
            List of properties to be queried. Refer MAPI documentation for
            supported properties.
        criteria: dict
            Criteria specified for the query. Uses Mongo-like flexible queries.
            Refer MAPI documentation for supported syntax, or available criteria.
        file_name: str
            Name of the file that stores data that is queried. This value is passed
            on to :meth:`write_query_to_file()`

        Returns
        -------
        code : int
            Status code to indicate different outcomes of the method.
        """

        MAPI_KEY = self.MAPI_KEY
        url = "https://materialsproject.org/rest/v2/query"
        data = {"criteria": criteria, "properties": properties}
        if not Path.exists(self.DATA_PATH / file_name):
            resp = post(
                url,
                headers={"X-API-KEY": MAPI_KEY},
                data={key: dumps(value) for key, value in data.items()},
            )
            # as suggested in https://github.com/materialsproject/mapidoc
            if resp.json()["valid_response"]:
                data = defaultdict()
                for comp in resp.json()["response"]:
                    data[comp["task_id"]] = comp
                self.DATA = data
                self.write_query_to_file(data, file_name=file_name)
                self.get_pymatgen_data(file_name=file_name)
                return 0
            else:
                return -1
        else:
            return 2

    def get_pymatgen_data(self, file_name) -> int:
        """Method to gather data available only through pymatgen.

        - **IMPORTANT**: The file must exist as this methods *adds* data to existent
        JSON file.

        Parameters
        ----------
        self : DataProcessing object
        file_name : str
            Name of the file.

        Returns
        -------
            : int
                A status code for reference.
        """
        data = self.DATA

        for key in list(data.keys()):
            data[key]["atomic_number"] = []
            data[key]["electronegativity"] = []
            data[key]["row"] = []
            data[key]["group"] = []
            data[key]["weights"] = []
            data[key]["atomic_radius"] = []
            data[key]["boiling_point"] = []
            data[key]["melting_point"] = []
            data[key]["atomic_mass"] = []
            data[key]["aiab_energy"] = []
            comp = Composition(str(data[key]["pretty_formula"]))
            for element_key, amount in comp.get_el_amt_dict().items():
                el_obj = Element(element_key)
                data[key]["atomic_number"].append(el_obj.Z)
                if isnan(el_obj.X):
                    del data[key]
                    continue
                else:
                    data[key]["electronegativity"].append(el_obj.X)
                    data[key]["row"].append(el_obj.row)
                    data[key]["group"].append(el_obj.group)
                    data[key]["atomic_radius"].append(el_obj.atomic_radius)
                    data[key]["weights"].append(comp.get_atomic_fraction(el_obj))
                    data[key]["boiling_point"].append(el_obj.boiling_point)
                    data[key]["melting_point"].append(el_obj.melting_point)
                    data[key]["atomic_mass"].append(el_obj.atomic_mass)
                    aiab_energy = self.get_element_aiab_energy(str(el_obj))
                    if aiab_energy is not None:
                        data[key]["aiab_energy"].append(
                            self.get_element_aiab_energy(str(el_obj))
                        )
                    else:
                        del data[key]
        self.DATA = data
        self.write_query_to_file(data, file_name=file_name)
        self.eval_per_atom_data(file_name)
        return 0

    def eval_per_atom_data(self, file_name) -> int:
        """Method to evaluate cohesive energy and volume per atom for a specimen.

        Parameters
        ----------
        self : DataProcessing object
        file_name : str
            Name of the file to save the data to.
        Returns
        -------
            : int
                Integer status indicator.
        """
        data = self.DATA
        for key in list(data.keys()):
            weighted_average_aiab = average(
                data[key]["aiab_energy"], weights=data[key]["weights"]
            )
            cohesive_energy = (
                float(data[key]["energy_per_atom"]) - weighted_average_aiab
            )
            data[key]["cohesive_energy"] = cohesive_energy

            vol_per_atom = log10(
                float(data[key]["volume"]) / float(data[key]["nsites"])
            )
            data[key]["vol_per_atom"] = vol_per_atom
        self.DATA = data
        self.write_query_to_file(data, file_name=file_name)
        return 0

    def get_elasticity_data(
        self, max_components=None, source_file_stub=None, dest_file_stub=None
    ) -> int:
        """Method to filter out elasticity specific data and to write it to a file.

        Parameters
        ----------
        max_components : int
            Maximum number of components in a specimen.
        source_file_stub : str
            Prefix of the file from which data is being sourced.
        dest_file_stub : str
            Prefix of the file to which data is to be written to.

        Returns
        -------
            : int
            A status indicator.
        """

        flag = False
        for ncomp in range(max_components):
            source_file = source_file_stub + f"_{ncomp+1}.json"
            dest_file = dest_file_stub + f"_{ncomp+1}.json"
            if Path.exists(self.DATA_PATH / dest_file):
                flag = True
                if self.verbose:
                    print(
                        f"Dependent variable data already exists at: {(self.DATA_PATH/dest_file)}"
                    )
            else:
                self.read_from_file(source_file)
                data = self.DATA
                dep_data = defaultdict()
                for key, value in data.items():
                    dep_data[str(key)] = defaultdict()
                    dep_data[str(key)]["K_VRH"] = value["elasticity"]["K_VRH"]
                    dep_data[str(key)]["G_VRH"] = value["elasticity"]["G_VRH"]
                    with errstate(invalid="ignore", divide="ignore"):
                        dep_data[str(key)]["log_K_VRH"] = log10(
                            value["elasticity"]["K_VRH"]
                        )
                        dep_data[str(key)]["log_G_VRH"] = log10(
                            value["elasticity"]["G_VRH"]
                        )
                self.write_query_to_file(data=dep_data, file_name=dest_file)
                if self.verbose:
                    print(
                        "Elasticity / Dependent data written to file at ",
                        self.DATA_PATH / dest_file,
                    )
                flag = True
        if flag:
            return 0
        else:
            return -1

    def write_query_to_file(self, data, file_name) -> None:
        """Method to write the result of a query out of \
        :meth:`query_property()` to a json file.

        Parameters
        ----------
        data : dict
            The resultant data dictionary of the :meth:`query_property()`, \
            :meth:`get_pymatgen_data()`, or :meth:`eval_per_atom_data()` methods.
        file_name: str
            Name of the file to be created as a string. File will be placed\
            in directory as specified in :attr:`DATA_PATH` attribute. 

        Returns
        -------
            : None
        File, if it doesn't exist, consisting of queried data is created as a \
            result of this method.
        """
        if self.verbose:
            print(f"Writing to file {file_name}")
        with open(self.DATA_PATH / file_name, "w+") as input_file:
            dump(data, input_file, indent=4)
