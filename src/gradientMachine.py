"""**Implementation of gradient boosting machine.**

- This file is a part of *Personal Programming Project (PPP)* coursework in \
Computational Materials Science (CMS) M.Sc. course in *Technische Universität \
Bergakademie Freiberg.*

- This file is a part of the project titled **Application of statistical learning \
to predict material properties.**

- For given data, methods present in this file apply Gradient Boosting algorithm \
to tune the model obtained from local regression. 
"""
from collections import defaultdict
from json import dump, load
from pathlib import Path
from timeit import default_timer as timer

import numpy as np

from localRegression import LocalRegression
from localRegressionCython import LocalRegressionCython
from localRegressionSimple import LocalRegressionSimple

__author__ = "Venkata Mukund Kashyap Yedunuthala"


class GradientBoostingMachine:
    def __init__(
        self,
        max_comps,
        verbose,
        init_iterations,
        learning_rate=None,
        K_flag=True,
        LRSimple_flag=True,
    ):
        """Implementation of Gradient Boosting Machine.

        Methods
        -------
        :meth:`squared_error` : Method to compute squared error. 
        :meth:`rmse` : Method to compute root mean squared error. 
        :meth:`load_data` : Method that takes prefixes of files and reads data present in
            the files. Sets attributes such as X, Y, task_ids etc. 
        :meth:`clean_data` : Method that strips the dataset of any invalid values that 
            might be present in it. 
        :meth:`eval_relative_influence` : Method that computes permutation influence 
            of features present in independent variables dataset. 
        :meth:`write_inf_data_to_disk` : Method that write the computed relative 
            influence data to disk in JSON format. 
        :meth:`n_fold_split` : Method that takes the input arrays to construct 
            given number of folds on permutated data. 
        :meth:`gradient_boosting_cv` : Method that initiates the gradient boosting 
            machine framework based computations. 
        :meth:`eval_gbm` : Method that performs the actual computations of 
            gradient boosting machine. 
        :meth:`predict` : Method that utilizes the boosted data matrix to 
            predict dependent data variables at given independent variables. 
        
        Attributes
        ----------
        max_comps : int
            Value indicating number of components in specimen, ranging
            between 1 and 4 in this implementation.
        verbose : bool
            To toggle printing information to terminal.
        learning_rate : float
            Rate at which the model obtained is attuned per iteration.
            Default value is 0.05
        DATA_PATH : str or PathLib.Path
            Path to directory that stores the data.
            Default value is "../data/"
        OUTPUT_PATH : str or PathLib.Path
            Path to directory that stores the outputs.
            Default value is "../output/"
        K_flag : bool
            Toggles problem between prediction of bulk modulus (K) or shear
            modulus (G). Default value sets to prediction of bulk modulus.
        LRSimple_flag : bool
            Toggles version of class LocalRegression to be used in the
            gradient boosting machine. Default value is LocalRegressionSimple
            which is faster.
        """
        self.max_comps = max_comps
        if learning_rate is None:
            self.learning_rate = 0.05
        else:
            self.learning_rate = learning_rate
        self.DATA_PATH = Path("../data")
        self.OUTPUT_PATH = Path("../output")
        self.verbose = verbose
        self.K_flag = K_flag
        self.init_iterations = init_iterations
        if LRSimple_flag:
            self.LocalRegression = LocalRegressionCython
        else:
            self.LocalRegression = LocalRegression
        self.log_iterations = False

    def squared_error(self, values, pred):
        """Method to compute squared error between two arrays.

        Parameters
        ----------
        pred : list or array
            Container for predicted variables.
        values : list or array
            Container for values being compared.

        Returns
        -------
        (se, sd) : tuple

            1. :obj:`se` returns an array of squared errors compared element
            wise between two containers.

            2. :obj:`sd` returns the standard deviation of this array of
            sqaured errors.


        """
        se = np.divide(np.square(values - pred), 2)
        # sd = np.std(se, ddof=1)
        return np.mean(se)

    def rmse(self, pred, actual):
        """Method to compute Root mean squared error (RMSE) between two arrays.

        Parameters
        ----------
        pred : list or array
            Container for predicted variables.
        actual : list or array
            Container for values being compared.

        Returns
        -------
        : float
            Computed RMSE.
        """
        return np.sqrt(((pred - actual) ** 2).mean())

    def load_data(self, dep_file_stub, indep_file_stub):
        """Method to load data from specified files. Calls
        :meth:`clean_data()` to strip data of unsupported data like NaN.

        Parameters
        ----------
        dep_file_stub : str
            String indicating prefix of the file containing dependent
            variables' data.
        indep_file_stub : str
            String indicating prefix of the file containing independent
            variables' data.

        Returns
        -------
        : int
            Status indicator. 0 indicates success, whereas -1 indicates contrary.
        """
        flag = False  # flag as a status indicator
        if self.K_flag:
            self.k_vrh_array = defaultdict()
        else:
            self.g_vrh_array = defaultdict()
        self.indep_array = defaultdict()
        self.data_indices = defaultdict()
        self.indep_params = defaultdict()
        self.task_ids = defaultdict()
        for ncomp in range(1, self.max_comps + 1):
            # loops to read for different numbers of components
            dep_file = dep_file_stub + f"_{ncomp}.json"
            indep_file = indep_file_stub + f"_{ncomp}.json"
            # reading dependent variables data
            with open(self.DATA_PATH / dep_file, "r") as ip:
                dep_data = load(ip)
                if self.K_flag:
                    self.k_vrh_array[ncomp] = np.empty(len(dep_data.keys()))
                else:
                    self.g_vrh_array[ncomp] = np.empty(len(dep_data.items()))
                for i, value in enumerate(dep_data.values()):
                    if self.K_flag:
                        self.k_vrh_array[ncomp][i] = value["log_K_VRH"]
                    else:
                        self.g_vrh_array[ncomp][i] = value["log_G_VRH"]
                self.task_ids[ncomp] = []
                for key in list(dep_data.keys()):
                    self.task_ids[ncomp].append(key)
                self.task_ids[ncomp] = np.array(self.task_ids[ncomp])
            # reading independent variables data
            with open(self.DATA_PATH / indep_file, "r") as ip:
                indep_data = load(ip)
                self.indep_array[ncomp] = np.empty(
                    (
                        len(indep_data.items()),
                        len(list(indep_data.values())[0]),
                    )
                )
                self.data_indices[ncomp] = np.arange(len(indep_data.items()))
                self.indep_params[ncomp] = np.array(
                    list(list(indep_data.values())[0].keys()), dtype=str
                )
            # setting default values to handle corrupted data if any.
            for i, data in enumerate(indep_data.values()):
                for j, value in enumerate(data.values()):
                    try:
                        self.indep_array[ncomp][i, j] = float(value)
                    except:
                        self.indep_array[ncomp][i, j] = np.nan
            flag = True
            if flag and self.verbose:
                print(
                    "Dependent variable data loaded for GBM from ",
                    self.DATA_PATH / dep_file,
                )
                print(
                    "Independent variable data loaded for GBM from ",
                    self.DATA_PATH / indep_file,
                )
        if flag:
            self.clean_data()
            return 0
        else:
            if self.verbose:
                print("Loading GBM data failed.")
            return -1

    def clean_data(self):
        """Method to remove invalid values from data arrays."""
        for ncomp in range(1, self.max_comps + 1):
            if self.K_flag:
                indices = np.isfinite(self.k_vrh_array[ncomp])
                self.k_vrh_array[ncomp] = self.k_vrh_array[ncomp][indices]
                self.indep_array[ncomp] = self.indep_array[ncomp][indices]
                self.task_ids[ncomp] = self.task_ids[ncomp][indices]
                for i in range(self.indep_array[ncomp].shape[1]):
                    indices = np.isfinite(self.indep_array[ncomp][:, i])
                    self.k_vrh_array[ncomp] = self.k_vrh_array[ncomp][indices]
                    self.indep_array[ncomp] = self.indep_array[ncomp][indices]
                    self.task_ids[ncomp] = self.task_ids[ncomp][indices]

                self.X = np.vstack(list(self.indep_array.values()))
                self.Y = np.concatenate(list(self.k_vrh_array.values()))
                self.task_ids_clean = np.concatenate(list(self.task_ids.values()))
            else:
                indices = np.isfinite(self.g_vrh_array[ncomp])
                self.g_vrh_array[ncomp] = self.g_vrh_array[ncomp][indices]
                self.indep_array[ncomp] = self.indep_array[ncomp][indices]
                self.task_ids[ncomp] = self.task_ids[ncomp][indices]
                for i in range(self.indep_array[ncomp].shape[1]):
                    indices = np.isfinite(self.indep_array[ncomp][:, i])
                    self.g_vrh_array[ncomp] = self.g_vrh_array[ncomp][indices]
                    self.indep_array[ncomp] = self.indep_array[ncomp][indices]
                    self.task_ids[ncomp] = self.task_ids[ncomp][indices]

                self.X = np.vstack(list(self.indep_array.values()))
                self.Y = np.concatenate(list(self.g_vrh_array.values()))
                self.task_ids_clean = np.concatenate(list(self.task_ids.values()))

            if self.verbose:
                print(f"Cleaned data for specimen with maximum components: {ncomp}")

    def eval_relative_influence(self, write_to_disk=False):
        """
        1. Method to evaluate relative influence of parameters on the model.

        2. Writes scores, relative scores, and corresponding parameters to a
        dict named :obj:`inf_data`


        Parameters
        ----------
        write_to_disk : bool
            Toggles writing evaluated information to disk. If true, calls
            :meth:`write_inf_data_to_disk()`

        Returns
        -------
        : int
            A status indicator. 0 indicates successful execution.
        """
        self.inf_data = defaultdict()  # initialising dict for results
        flag = False  # status indicator
        for ncomp in range(1, self.max_comps + 1):
            x = self.indep_array[ncomp]
            if self.K_flag:
                y = self.k_vrh_array[ncomp]
            else:
                y = self.g_vrh_array[ncomp]
            # fitting an initial model to estimate rmse
            model = self.LocalRegression()
            model.fit(x, y)
            current_rmse = self.rmse(model.predict(x), y)
            n_params = x.shape[1]
            scores = np.empty(n_params)
            # for each parameter, its values are shuffled to
            # estimate a new rmse
            for j in range(n_params):
                x_j = x.copy()
                x_j[:, j] = np.random.permutation(x_j[:, j])
                model.fit(x=x_j, y=y)
                new_rmse = self.rmse(model.predict(x_j), y)
                scores[j] = np.abs(new_rmse - current_rmse)
            # calculate relative influence by division with
            # highest score
            relative_scores = scores / scores.max()
            sorted_indices = np.argsort(scores)[::-1]  # descending order
            self.inf_data[ncomp] = {
                "scores": scores[sorted_indices],
                "relative scores": relative_scores[sorted_indices],
                "parameters": self.indep_params[ncomp][sorted_indices],
            }
            flag = True
        if self.verbose and flag:
            print("Evaluated relative influence of parameters.")
            # print("-"*80)
        if write_to_disk:
            self.write_inf_data_to_disk(ncomp)
        if flag:
            return 0
        else:
            return -1

    def write_inf_data_to_disk(self, ncomp):
        """Method to write influence data to disk, toggled by
        :meth:`eval_relative_influence()`.

        Parameters
        ----------
        ncomp : int
            Integer indicating number of components in the specimen.

        Returns
        -------
        : int
            Status indicator. 0 suggests successful execution.
        """
        flag = False
        data_dict = {
            "Number of data points": self.indep_array[ncomp].shape[0],
            "Number of parameters": self.indep_array[ncomp].shape[1],
        }
        data_dict["Scores"] = defaultdict()
        for n in range(self.inf_data[ncomp]["scores"].shape[0]):
            data_dict["Scores"][self.inf_data[ncomp]["parameters"][n]] = {
                "Relative Influence": self.inf_data[ncomp]["relative scores"][n],
                "Feature Influence": self.inf_data[ncomp]["scores"][n],
            }
        if self.verbose:
            file = "influence_" + str(ncomp)
            print(f"Writing relative influence data to disk at {self.OUTPUT_PATH/file}")
        with open(self.OUTPUT_PATH / f"influence_{ncomp}.json", "w+") as f:
            dump(data_dict, f, indent=4)
            f.close()
        if flag:
            return 0
        else:
            return 1

    def n_fold_split(self, data_tuple, folds):
        """Method to split given data into given number of folds.

        Parameters
        ----------
        data_tuple : tuple
            Tuple in format (X,Y)
        folds : int
            Number of folds.

        Returns
        -------
        split_data : dict
            Dict consisting of split values of x and y with integers as keys.
        """
        x_set = np.array(data_tuple[0])
        y_set = np.array(data_tuple[1])
        if x_set.shape[0] == y_set.shape[0]:
            N = y_set.shape[0]
        else:
            raise ValueError("Shape of arguments does not match")
        indices = np.random.permutation(np.arange(N))
        x_split = np.array_split(x_set[indices], folds)
        y_split = np.array_split(y_set[indices, np.newaxis], folds)
        split_data = defaultdict()
        for f in range(folds):
            x_copy = x_split.copy()
            y_copy = y_split.copy()
            split_data[f] = {
                "x_test": x_copy.pop(f),
                "x_train": np.vstack(x_copy),
                "y_test": y_copy.pop(f),
                "y_train": np.vstack(y_copy),
            }
        self.split_data = split_data
        return split_data

    def gradient_boosting_cv(
        self,
        folds=10,
        full_dataset=False,
        log_iterations=False,
    ):
        """Implementation of gradient boosting iterations within a cross-validation
        framework.

        - If verbose, prints out in-sample and out-of-sample mean squared-error for each fold.

        - Sets various attributes that are helpful in post-processing.


        Parameters
        ----------
        x : list or array
            Independent variables that are used to estimate dependent variables.
        y : list or array
            Dependent variables that are available from the data.
        init_iterations : int
            Number of GBM iterations.
        folds : int
            Number of folds into which the data is split into.

        Returns
        -------
        : None
        """
        properties = np.array(
            ["vol_per_atom", "row_1", "cohesive_energy", "electronegativity_neg_4"]
        )
        # self.clean_data()
        prop_indices = np.isin(self.indep_params[1], properties)
        # X = self.indep_array[1][:, prop_indices]
        # Y = self.k_vrh_array[1]
        X = self.X[:, prop_indices]
        Y = self.Y
        split_data = self.n_fold_split((X, Y), folds=folds)
        if self.verbose:
            print("=" * 80)
            print("GBM Iterations")
            print("-" * 80)
        if full_dataset:
            y_pred = self.eval_gbm(
                x_train=self.X[:, prop_indices], y_train=Y, full_dataset=True, log_iterations=log_iterations
            )
            return y_pred
        else:
            for fold in range(folds):
                x_train = split_data[fold]["x_train"]
                y_train = split_data[fold]["y_train"].ravel()
                x_test = split_data[fold]["x_test"]
                y_test = split_data[fold]["y_test"].ravel()
                y_out = self.eval_gbm(x_train, y_train, fold=fold, full_dataset=False)

    def eval_gbm(
        self, x_train, y_train, fold=None, full_dataset=None, log_iterations=False
    ):
        """Method for the actual computations of boosting.

        Parameters
        ----------
        x_train : array
            Array consisting independent variables.
        y_train : array
            Array consisting of available dependent variables.
        fold : int
            Indicator for the number of fold being evaluated, given only when
            boosting is not being performed on the full dataset.
        full_dataset : bool
            Toggle for evaluation using full dataset.

        Returns
        -------
        y_i : array
            Array consisting fitted values of dependent variables.
        """
        start = timer()
        init_iterations = self.init_iterations
        learner = LocalRegressionSimple(frac=0.33)
        learner.fit(x_train, y_train)
        y_0 = learner.predict(x_train)  # initial guess
        if self.verbose:
            if full_dataset:
                print(f"Dimensions of training data: {x_train.shape}, {y_train.shape}")
                print(f"Initial MSE in-sample: {self.squared_error(y_train,y_0)}")
                print(f"Time elapsed: {timer()-start:0.4f} seconds")
            else:
                print(
                    f"Initial MSE in-sample for fold {fold+1}: {self.squared_error(y_train,y_0)}"
                )
                # print(f"Initial MSE out-of-sample for fold {fold+1}: {self.squared_error(y_test,learner.predict(x_test))}")
        y_i = y_0
        d_mat_n = learner.d_mat
        alpha = 1
        lr = self.learning_rate
        for i in range(init_iterations):
            r_i = y_train - y_i  # negative gradient
            learner.fit(x_train, r_i)
            pred_i = learner.predict(x_train)
            # gradient descent step
            multiplier = alpha
            next_mul = multiplier
            gd_rate = 0.01
            gd_pre = 1e-6
            gd_max_it = 100
            for k in range(gd_max_it):
                cur_mul = next_mul
                next_mul = cur_mul - (
                    gd_rate * (cur_mul * pred_i + np.multiply(r_i, pred_i))
                )
                step = next_mul - cur_mul
                if np.any(np.abs(step)) <= gd_pre:
                    break
                multiplier = next_mul
            y_i = y_i + (lr * multiplier * pred_i)
            d_mat_n = d_mat_n + (
                lr * np.multiply(multiplier[:, np.newaxis], learner.d_mat)
            )
            if self.verbose and log_iterations:
                print(f"GBM iteration: {i}")
                print(f"MSE in-sample: {self.squared_error(y_train, y_i)}")
                print(f"Time elapsed: {timer() - start:0.4f} seconds")
                print("-" * 80)
        if self.verbose:
            if full_dataset:
                print(
                    f"Final MSE in-sample after GBM: {self.squared_error(y_train,y_i)}"
                )
            else:
                print(
                    f"Final MSE in-sample after GBM for fold {fold+1}: {self.squared_error(y_train,y_i)}"
                )
            # print(f"Final MSE out-of-sample for fold {fold+1}: {self.squared_error(y_test,learner.predict(x_test,d_mat=d_mat_n))}")
            print("-" * 80)
        return y_i

    def predict(self, x, d_mat):
        """Method to predict newer values of dependent variables using learned data matrix.

        Parameters
        ----------
        x : list or array
            Array consisting of independent variables to be used.
        d_mat : list or array
            Learned data matrix.

        Returns
        -------
        : array
            Predicted dependent variables.
        """
        model = self.LocalRegression()
        return model.predict(x, d_mat=d_mat)