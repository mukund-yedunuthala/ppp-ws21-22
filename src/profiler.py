import cProfile
import pstats
from pathlib import Path
from train import main
from io import StringIO
from numpy import genfromtxt
out_path = Path("../output/")

def gen_profile():
    """ Function that generates the profile for main() in train.py
    Outputs a file that places itself in the ../output directory.
    """
    p_inst = cProfile.Profile()
    p_inst.enable()
    main()
    p_inst.disable()
    stats = pstats.Stats(p_inst).sort_stats("cumtime")
    stats.strip_dirs()
    stats.sort_stats('cumulative')
    stats.dump_stats(out_path/"profile.prof")

def analyze_profile():
    """ Function that analyzes the generated profile found in the ../output directory.
    
    Returns
    -------
    profile_data : data stored in profile file as a dictionary
    stats : stats sorted by cumulative time taken. 
    """
    stats = pstats.Stats((out_path/"profile.prof").__str__())
    stats = stats.sort_stats("cumtime")
    profile_data = stats.__dict__
    return profile_data, stats

def gen_np_array():
    """Function that converts the obtained data into a numpy array. 

    Returns
    -------
    data : numpy array
        Consists of data sorted by cumulative time.  
    """
    res = StringIO()
    pstats.Stats((out_path/"profile.prof").__str__(),stream=res).print_stats()
    res = res.getvalue()
    res = "ncalls" + res.split("ncalls")[-1]    
    res = '\n'.join([';'.join(line.rstrip().split(None,5)) for line in res.split('\n')])
    data = genfromtxt(
        fname=StringIO(res),
        delimiter = ";",
        autostrip=True,
        encoding='utf-8',
        dtype=None,
        names=True
    )
    return data
    