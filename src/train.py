from collections import defaultdict
from json import dump, load

from dataProcessing import DataProcessing
from gradientMachine import GradientBoostingMachine
from hoelderMeans import HoelderMeans
from plot import Plots


def main():
    max_components = 4
    indep_data_file_stub = "indep_ncomp"
    dep_data_file_stub = "dep_ncomp"
    source_data_file_stub = "data_ncomp"
    criteria = {"elasticity": {"$exists": True}}
    learning_rate = 0.05
    n_folds = 10
    init_iterations = 1

    # Flags
    verbosity = True
    check_api = False
    K_flag = False
    LR_Simple_flag = True
    full_dataset = True
    log_iterations = True
    dark_background_plot = False
    transparent_plot = True
    show_plot = False

    data_object = DataProcessing(verbose=verbosity, check_api=check_api)
    with open(data_object.DATA_PATH / "properties.json", "r") as ip:
        properties = load(ip)
    queries = properties["queries"]

    data_object.collect_data_from_source(
        properties=queries,
        criteria=criteria,
        file_name="data_ncomp",
        max_components=max_components,
    )
    data_object.get_elasticity_data(
        max_components=max_components,
        source_file_stub=source_data_file_stub,
        dest_file_stub=dep_data_file_stub,
    )
    comp_descriptors = properties["composition_descriptors"]
    means = HoelderMeans(
        comp_descriptors=comp_descriptors,
        verbose=verbosity,
        source_file=source_data_file_stub,
        file=indep_data_file_stub,
        max_components=max_components,
    )

    model = GradientBoostingMachine(
        max_comps=max_components,
        verbose=verbosity,
        learning_rate=learning_rate,
        init_iterations=init_iterations,
        K_flag=K_flag,
        LRSimple_flag=LR_Simple_flag,
    )
    model.load_data(
        dep_file_stub=dep_data_file_stub, indep_file_stub=indep_data_file_stub
    )
    y_pred = model.gradient_boosting_cv(
        folds=n_folds, full_dataset=full_dataset, log_iterations=log_iterations
    )


    output_data = defaultdict()
    for i in range(model.task_ids_clean.shape[0]):
        output_data[model.task_ids_clean[i]] = y_pred[i]
    if K_flag:
        with open(
            model.OUTPUT_PATH / f"predicted_log_k_vrh_{max_components}.json", "w+"
        ) as opt:
            dump(output_data, opt, indent=4)
    else:    
        with open(
            model.OUTPUT_PATH / f"predicted_log_g_vrh_{max_components}.json", "w+"
        ) as opt:
            dump(output_data, opt, indent=4)
    plots = Plots(
        dark_background=dark_background_plot,
        transparent=transparent_plot,
        show=show_plot,
    )
    if K_flag:
        plots.plot_comparison(max_components=max_components, predicted="K")
    else:
        plots.plot_comparison(max_components=max_components, predicted="G")
        


if __name__ == "__main__":
    main()
