"""**Implementation of Multi-variate local polynomial regression.**

- This file is a part of *Personal Programming Project (PPP)* coursework in \
Computational Materials Science (CMS) M.Sc. course in *Technische Universität \
Bergakademie Freiberg.*
- This file is a part of the project titled **Application of statistical learning \
to predict material properties.**
- For a given number of data points, the algorithm fits a polynomial of given \
degree to the data points in a specified neighbourhood. 
- Given response variables and predictor variables, this can be used to estimate \
a regression function. 
- At fitting points the underlying function is assumed to be smooth and k-times \
differentiable to fit a polynomial of degree k.

References
----------
    Cleveland, W.S. (1979) "Robust Locally Weighted Regression
    and Smoothing Scatterplots". Journal of the American Statistical
    Association 74 (368): 829-836.

"""

from itertools import product, repeat
from warnings import warn

import numpy as np

__author__ = "Venkata Mukund Kashyap Yedunuthala"


class LocalRegression:
    """Multi-variate local polynomial regression.

    Methods
    -------
    :meth:`tricubic_weights` : Method that computes the weights from the
        normalized distances within the window. This method specifically
        returns values of tricube weighting function evaluation.

    :meth:`eval_cart_prod` : Method that utilizes itertools to build
        bases that would later be used in least squares method to estimate
        the function.

    :meth:`fit_polynomial` : Method that uses NumPy's least squares method to
        output the data matrix that is eventually used to
        evaluate the predictor variable at that point.

    :meth:`fit` : Method that provides the initial wrapper for the methods
        mentioned above.

    :meth:`predict` : Method that utilizes the learned data matrix to
        evaluate the predictor variables.

    Attributes
    ----------
    alpha : float
        Fraction of data to be used while estimating polynomial.
        Value must be between 0 or 1.
    degree : int
        Degree of the polynomial to be fit to given data.
        Defaults to 1, corresponding to linear polynomial, in this particular
        implementation.

    """

    def __init__(self, alpha, degree=1) -> None:
        self.alpha = alpha
        self.degree = degree

    def tricubic_weights(args):
        """Method that assign weights to points in the local neighbourhood \
        of x-values.

        It provides more weight to observations whose value is closer \
        to the given point, and less weight to observations that are \
        further away. 

        Parameters
        ----------
        args: list or array
            Weight function is dependent on the distance in the neighbourhood, \
            with it being zero outside of the local neighbourhood. This \
            contains the distances evaluated earlier.

        Returns
        -------
        weights : array
            Assigned weights to the corresponding data points in the vicinity. 
        """
        args = np.asarray(args, dtype=float)
        weights = np.zeros_like(args, dtype=float)
        positions = np.where(np.abs(args) <= 1)
        weights[positions] = (1 - args[positions] ** 2) ** 3
        return weights

    def eval_cart_prod(self, x, x_values):
        if len(x.shape) == 1:
            x = x.reshape(-1, 1)
        if len(x_values.shape) == 1:
            x_values = x_values.reshape(-1, 1)
        n_data, n_params = x.shape
        n_evalpts, n_ = x_values.shape
        bases = product(*repeat(np.arange(self.degree + 1), n_params))
        bases = np.array(list(filter(lambda a: sum(a) <= self.degree, bases)))
        X = np.ones((n_data, len(bases)))
        X_values = np.ones((n_evalpts, len(bases)))
        for i in range(len(bases)):
            X[:, i] = np.product(x[:, :] ** bases[i, :], axis=1)
            X_values[:, i] = np.product(x_values[:, :] ** bases[i, :], axis=1)
        return X, X_values

    def fit_polynomial(self, x, y, x_values, weights, degree):
        """Method that performs computationally intensive task to return \
        the polynomial value.

        Parameters
        ----------
        x : list or array
            Predictor variables 'X'.
        y : list or array
            Response variables 'Y'.
        x_values : list or array
            Values of x at which the polynomial is fit. 
        weights: list or array
            Weights assigned to corresponding data points in the neighbourhood.
        degree: int
            Degree of polynomial being fit.

        Returns
        -------
        scalar : float 
            The fitted response variable 'Y' which can be interpolated to predict \
            for newer data points.

        """
        n_data, n_params = x.shape
        n_evalpts, n_ = x_values.shape

        if len(x) == 0:
            return np.nan * np.ones(n_evalpts)

        if weights is None:
            weights = np.ones(n_data)

        sqrt_weights = np.sqrt(weights)

        X, X_values = self.eval_cart_prod(x, x_values)

        left_variables = X * sqrt_weights[:, None]
        right_variables = y * sqrt_weights

        rcond = np.finfo(left_variables.dtype).eps * max(*left_variables.shape)

        output_vars = np.linalg.lstsq(left_variables, right_variables, rcond=rcond)[0]

        return X_values.dot(output_vars), output_vars

    def fit(self, x, y, kernel=tricubic_weights):
        """Method that performs multi-variate local polynomial regression. 

        Parameters
        ----------
        kernel : function
            Weighting function to be used during this evaluation. Default is \
            tricubic weighting function as described in references. These are \
            expected to give higher weight to observations near the smoothing \
            window and zero weight to those outside.

        Returns
        -------
        output_values : array
            All values of fit polynomial according to given parameters. Can be \
            interpolated to evaluate for newer data points. 
        """
        x = np.asarray(x, dtype=float)
        y = np.asarray(y, dtype=float)
        # choosing evaluation points to be same as predictor data points
        x_values = x
        # flattening in case of single variable
        if len(x.shape) == 1:
            x = x.reshape(-1, 1)
            x_values = x_values.reshape(-1, 1)

        n_data, n_feats = x.shape
        n_opt, n_opt_feats = x_values.shape
        output_values = np.zeros(n_opt, dtype=float)
        betas_array = []
        weights_array = []
        # fraction cannot be none, as division of data points into subsets
        # is the core feature of local regression.
        if self.alpha is None:
            raise ValueError("Local Regression requires choosing subset fraction")

        else:
            subset_size = int(self.alpha * n_data)
            for i, xi in enumerate(x_values):
                # calculating distance in the neighbourhood to assign weights
                distance = np.linalg.norm(x - xi[None, :], axis=1)
                positions = np.argsort(distance)[:subset_size]
                # this is to normalize the distances in the subset between 0 and 1
                radius = distance[positions][-1]
                weights = kernel(distance[positions] / radius)
                weights_array.append(weights)
                # fit_polynomial performs the computationally expensive task
                # TODO: profiler here.
                output_values[i], betas = self.fit_polynomial(
                    x=x[positions],
                    y=y[positions],
                    x_values=xi[None, :],
                    weights=weights,
                    degree=self.degree,
                )
                betas_array.append(betas)
        self.train_weights = np.array(weights_array)
        self.design_matrix = np.array(betas_array)
        if np.any(np.isnan(output_values)):
            warn("Unsupported values")

        self.output_values = output_values

    def predict(self, x_pred, design_matrix=None):
        if design_matrix is None:
            design_matrix = self.design_matrix
        x_pred = np.asarray(x_pred, dtype=float)
        y_pred = np.zeros(x_pred.shape[0])
        x_eval = x_pred.copy()
        if len(x_pred.shape) == 1:
            x_pred = x_pred.reshape(-1, 1)
            x_eval = x_eval.reshape(-1, 1)
        n_data, n_para = x_pred.shape
        out_data, out_para = x_eval.shape
        for i, xi in enumerate(x_eval):
            x, x_e = self.eval_cart_prod(x_pred, xi[None, :])
            y_pred[i] = x_e.dot(design_matrix[i].reshape(-1, 1))
        return y_pred
