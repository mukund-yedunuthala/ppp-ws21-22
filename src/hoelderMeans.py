"""**Calculate hoelder means of various descriptors**

- Takes elemental property values and calculates the Hoelder means for given \
exponentials, could also calculate weighted Hoelder means. 
- This file is a part of *Personal Programming Project (PPP)* coursework in \
Computational Materials Science (CMS) M.Sc. course in *Technische Universität \
Bergakademie Freiberg*. 
This file is a part of the project titled **Application of statistical learning \
to predict material properties.**
"""

from collections import defaultdict
from json import dump
from pathlib import Path
from warnings import warn

import numpy as np

from dataProcessing import DataProcessing

__author__ = "Venkata Mukund Kashyap Yedunuthala"
__date__ = "October 26, 2021"
LOWER_LIMIT = 1e-12


class HoelderMeans:
    """Class to represent Hoelder means of descriptors.

    Methods
    -------
    :meth:`eval_means()` : Method that acts as a wrapper for other methods,
        it checks the availability on the disk and then calls relevant
        methods to either notify or compute new data and write to files.
    :meth:`get_alpha()` : Method to compute a multiplier present in the
        computation of generalized means.
    :meth:`compute_means()` : Method that performs the computations of
        generalized data.
    :meth:`combine_dft_data()` : Method that uses available source data to
        add the data obtained through DFT post-processing to the computed
        generalized means of composition descriptors. This completes the
        dataset of independent variables.
    :meth:`write_to_file()` : Method that writes computed data to
        disk with a given file name.

    """

    def __init__(
        self, comp_descriptors, verbose, source_file, file, max_components
    ) -> None:
        """To instantiate an object of class :class:`HoelderMeans`

        Attributes
        ----------
        comp_descriptors : list
            List consisting of composition descriptors. These are calculated from
            elemental properties.
        verbose : bool
            Indicates level of verbosity.
        source_file : str
            Stub of the source data file.
        file : str
            Stub of the destination data file
        DATA_PATH : Path
            Path to directory that houses data file.
        """
        self.DATA_PATH = Path("../data/")
        self.comp_descriptors = comp_descriptors
        self.verbose = verbose
        self.file_stub = file
        self.max_components = max_components
        for ncomp in range(1, max_components + 1):
            file_name = file + f"_{ncomp}.json"
            source = source_file + f"_{ncomp}.json"
            if Path.exists(self.DATA_PATH / file_name):
                self.file_flag = True
                if self.verbose:
                    if self.file_flag:
                        print(
                            "Independent variable data already exists at ",
                            self.DATA_PATH / file_name,
                        )
            else:
                print(
                    f"Computing Hoelder means for compositional descriptors: \
                    {self.comp_descriptors}, for systems with components: {ncomp}"
                )
                self.eval_means(source, file_name)

    def eval_means(self, source, file_name, exponents=np.arange(-4, 5)) -> int:
        """To evaluate Hoelder means of the given data.

        Attributes
        ----------
        self : Object of class HoelderMeans
        exponents : NDArray
            Array of exponents upon which means are evaluated.

        Returns
        -------
            : int
            Status indicator.
        """
        dataProc = DataProcessing(check_api=False)
        dataProc.read_from_file(file=source)
        data = dataProc.DATA
        self.DATA = dataProc.DATA
        comp_descriptors = self.comp_descriptors
        exponents = exponents
        data_hoelder = defaultdict()
        flag = False
        for key in list(data.keys()):
            pl_dict = defaultdict()
            for exponent in exponents:
                for desc in comp_descriptors:
                    if exponent < 0:
                        pl_dict[desc + "_neg_" + str(-exponent)] = self.compute_mean(
                            data[key][desc],
                            exponent=exponent,
                            weights=data[key]["weights"],
                        )
                    else:
                        pl_dict[desc + "_" + str(exponent)] = self.compute_mean(
                            data[key][desc],
                            exponent=exponent,
                            weights=data[key]["weights"],
                        )
                    data_hoelder[data[key]["task_id"]] = pl_dict
            flag = True
        self.data_hoelder = data_hoelder
        self.write_to_file(file_name)
        self.combine_dft_data(file_name)
        if flag:
            return 0
        else:
            return -1

    def get_alpha(self, weights, norm_method, values) -> float:
        """Computes a part of the equation to get Hoelder means.

        Parameters
        ----------
        weights: list or array
            List of array of weights
        norm_method: str
            Normalization method.
        values: list or array
            Values upon which the means are to be calculated

        Returns
        -------
        alpha: float
            Computed first part of the equation.
        """

        if weights is None:
            alpha = 1 / len(values)
        else:
            weights = np.array(weights, dtype=np.float64)
            if len(values) != len(weights):
                warn("WARNING: Sizes of values and weights mismatch")
                return 0.00
            if norm_method is not None:
                if norm_method == "max" and max(weights) != 1.00:
                    weights = weights / max(weights)
                elif norm_method == "sum" and sum(weights) != 1.00:
                    weights = weights / sum(weights)
                else:
                    warn("WARNING: Unknown normalization method")
                    return 0.00
            alpha = 1 / sum(weights)
        return alpha

    def compute_mean(
        self, values, weights=None, exponent=None, alpha=None, norm_method=None
    ) -> float:
        """Compute the hoelder mean of the values provided per given
        exponent.

        Parameters
        ----------
        values: list or array
            Values upon which means are computed.
        weights: list or array
            Optional array of weights to be considered.
        exponent: float
            Exponent corresponding to Hoelder means.
        alpha: float
            First part of equation computed by get_alpha() method.
        norm_method: str
            Normalization method.
        Returns
        -------
        : float
            Computed mean
        """

        values = np.array(values)
        alpha = self.get_alpha(weights, norm_method, values)
        if weights is not None:
            weights = np.array(weights, dtype=np.float64)

        if exponent is None:
            raise ValueError("Invalid exponent value")
        else:
            exponent = float(exponent)

        if exponent == 0.00:
            for value in values:

                if value is None:
                    return None

                elif abs(value) < LOWER_LIMIT:
                    self.value = 0.00
                    return self.value
                elif value < 0:
                    warn("WARNING: Negative value with exponent 0")
                    self.value = 0.00
                    return self.value
            if weights is None:
                self.value = np.exp(alpha * np.sum(np.log(values)))
                return self.value
            else:
                self.value = np.exp(alpha * sum(weights * np.log(values)))
                return self.value

        elif exponent == 1.00:
            try:
                self.value = np.average(values, weights=weights)
                return self.value
            except:
                return None

        for value in values:

            if value is None:
                return None

            elif (value < 0) and (exponent % 2 != 0.0):
                warn("WARNING: Negative value with odd exponent")
                self.value = 0.00
                return self.value

        if weights is None:
            self.value = np.power(
                alpha * np.sum(np.power(values, exponent)), 1 / exponent
            )
            return self.value
        else:
            self.value = np.power(
                alpha * np.sum(weights * np.power(values, exponent)), 1 / exponent
            )
            return self.value

    def combine_dft_data(self, file_name):
        """Combines structural descriptors data from DFT and post-processing with
        Hoelder means data obtained for composition descriptors.

        Parameters
        ----------
        self : object of class DataProcessing

        Returns
        -------
            : int
            Status indicator.
        """

        properties = [
            "cohesive_energy",
            "formation_energy_per_atom",
            "band_gap",
            "e_above_hull",
            "density",
            "vol_per_atom",
        ]

        for prop in properties:
            for key, value in self.data_hoelder.items():
                value[prop] = self.DATA[key][prop]

        return self.write_to_file(file_name)

    def write_to_file(self, file_name=None) -> int:
        """Method to write data to file.

        Parameters
        ----------
        self : Object of :class:`HoelderMeans`

        Returns
        -------
            : int
            A status indicator.
        """
        if self.verbose:
            print(f"Writing Hoelder means data to file {self.DATA_PATH/file_name}")

        data = self.data_hoelder
        file = file_name
        flag = False
        with open(file=self.DATA_PATH / file, mode="w") as op:
            dump(data, op, indent=4)
            flag = True
        if flag:
            return 0
        else:
            return -1
