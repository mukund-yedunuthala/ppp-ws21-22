"""**Various helper plot functions used in the project**

- Primarily uses matplotlib.pyplot. 
- This file is a part of *Personal Programming Project (PPP)* coursework in \
Computational Materials Science (CMS) M.Sc. course in *Technische Universität \
Bergakademie Freiberg.*
- This file is a part of the project titled **Application of statistical learning \
to predict material properties.**
"""
from collections import defaultdict
from pathlib import Path
from json import load
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
__author__ = "Venkata Mukund Kashyap Yedunuthala"

class Plots:
    """Class to host helper plot methods."""

    def __init__(self, show=False, dark_background=False, transparent=False) -> None:
        self.dark = dark_background
        self.OUT_PATH = Path("../output")
        self.DATA_PATH = Path("../data")
        self.metadata = {"Author": "Venkata Mukund Kashyap Yedunuthala"}
        self.transparent = transparent
        self.show = show

    def plot_comparison(self, max_components, predicted='K'):
        dt = datetime.now()
        op_file = f"predicted_log_{predicted.lower()}_vrh_{max_components}.json"
        with open(self.OUT_PATH/op_file,"r") as op:
            output_data = load(op)
        x_values = defaultdict()
        y_values = defaultdict()
        legend = ['Unary','Binary','Ternary','Quarternery']
        marker = ['.','^','1','s']
        fig = plt.figure(figsize=(8,6))
        ax = plt.axes()

        for i in range(1,max_components+1):
            x_values[i] = []
            y_values[i] = []
            with open(self.DATA_PATH/f"dep_ncomp_{i}.json","r") as ip:
                input_data = load(ip)
            for key,value in list(input_data.items()):
                if key in output_data.keys():
                    x_values[i].append(value[f"log_{predicted}_VRH"])
                    y_values[i].append(output_data[key])
            ax.scatter(
                10**np.array(x_values[i]),
                10**np.array(y_values[i]),
                s = 7,
                label = legend[i-1],
                marker = marker[i-1]
            )
        lims = [
        np.min(ax.get_ylim()),  # min of both axes
        np.max(ax.get_ylim()),  # max of both axes
        ]
        ax.plot(lims,lims,'--',c='tab:grey')
        ax.set(
            xlabel = f'{predicted}'+r'$_{DFT}$ (GPa)',
            ylabel = f'{predicted}'+r'$_{GBM}$ (GPa)',
            title = f"Comparison of DFT training data with GBM predictions for {predicted}",
            aspect = 'equal',
            xlim = lims,
            ylim = lims,
        )
        ax.grid()
        plt.legend()
        if self.show:
            plt.show()
        else:
            fig.savefig(
                self.OUT_PATH/f"comparison_{predicted}_{max_components}.png",
                dpi = 600,
                transparent = self.transparent,
                metadata = {
                    'Author' : "Venkata Mukund Kashyap Yedunuthala",
                    'Creation time' :  dt.strftime('%A %d-%m-%Y, %H:%M:%S')
                }
            )
