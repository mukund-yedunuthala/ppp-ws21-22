# Gradient Boosting Machine with Local Regression weak learner

- This file is a part of **Personal Programming Project (PPP)** coursework in _Computational Materials Science (CMS) M.Sc._ course in _Technische Universität Bergakademie Freiberg._ 

- Submitted in Winter Semester of 2021/2022

- Documentation available at: [mukund-yedunuthala.gitlab.io/ppp-ws21-22/](https://mukund-yedunuthala.gitlab.io/ppp-ws21-22/)
